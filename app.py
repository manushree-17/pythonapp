from flask import Flask
import os

app = Flask(__name__)

@app.route("/")
def skill():
    message = "My name is {name}. I am learning Git and GitLab."
    return message.format(name=os.getenv("NAME", "Manushree"))
    
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
